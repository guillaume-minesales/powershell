#Allowing RDS + ping
Set-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\Terminal Server" -Name "fDenyTSConnections" -Value 0
New-NetFirewallRule -DisplayName "RDS" -Direction Inbound -LocalPort 3389 -Protocol TCP -Action Allow
New-NetFirewallRule -DisplayName "Ping" -Protocol ICMPv4 -IcmpType 8 -Enabled True -Profile Any -Action Allow

write-output "Rules Created"