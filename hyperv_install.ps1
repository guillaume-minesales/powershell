# Deploying HyperV

$hote = $env:computername

write-output $hote 

$confirmation = Read-Host "Do you want to install Hyper-V? (y/n)"

# Install
if ($confirmation -ceq 'y') {
    Install-WindowsFeature -Name Hyper-V -ComputerName $hote -IncludeManagementTools   
}

# Reboot after deploy
$confirmation = Read-Host "You need to restart the server, do you want to do it now? (y/n)"
if ($confirmation -ceq 'y') {
    Restart-computer $hote
}