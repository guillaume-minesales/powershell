$featuresnmp = "SNMP-Service"
$featurersat = "RSAT-SNMP"
$manager = "192.168.1.247"
$commstring = "cmist"

# Import ServerManager Module
Import-Module ServerManager

# Installer features nécessaires
$check = Get-WindowsFeature | Where-Object {$_.Name -eq $featuresnmp}
If ($check.Installed -ne "True")
{
Add-WindowsFeature $featuresnmp | Out-Null
Add-WindowsFeature $featurersat | Out-Null
Add-WindowsFeature -Name SNMP-Service -IncludeAllSubFeature -IncludeManagementTools �Restart
$check = Get-WindowsFeature | Where-Object {$_.Name -eq $featuresnmp}
}
If ($check.Installed -eq "True")
{
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\SNMP\Parameters\PermittedManagers" /v 2 /t REG_SZ /d $manager /f
Foreach ( $string in $commstring )
{
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\SNMP\Parameters\ValidCommunities" /v $string /t REG_DWORD /d 4 /f
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\SNMP\Parameters\TrapConfiguration$string" /v 1 /t REG_SZ /d $manager /f
}
# Démarrage/redémarrage du service SNMP si non fait
Stop-Service "SNMP" >> .logScript.txt
Start-Service "SNMP" >> .logScript.txt
}