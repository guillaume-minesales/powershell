# Powershell

Automation for Windows Server app and services deployment


# First step

In order to allow scripts execution, launch the following command in a powershell shell : 

`set-executionpolicy unrestricted`

This command allows script execution. It's recommended, once preparation done, to set it back to an higher security level :

`set-executionpolicy restricted`