﻿$Username = "Plop"

$Password = ConvertTo-SecureString -String "Plop123456@" -AsPlainText -Force 

$Groupe   = "Administrateurs"

NET USER $Username $Password /add /y /expires:never

NET LOCALGROUP $Groupe $Username /add

Set-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\Terminal Server" -Name "fDenyTSConnections" –Value 0

New-NetFirewallRule -DisplayName "RDS" -Direction Inbound -LocalPort 3389 -Protocol TCP -Action Allow