$CurrentUser = $env:UserName

# Configure VM Folder path
New-Item -Path "E:\Hyper-V" -ItemType Directory

Set-VMHost -VirtualHardDiskPath 'E:\Hyper-V\Virtual Hard Disks'
Set-VMHost -VirtualMachinePath 'E:\Hyper-V'



# Create Hyper Shortcut on desktop

Copy-Item "C:\Windows\System32\virtmgmt.msc" -destination "C:\Users\$CurrentUser\Desktop\Hyper-V Manager.msc"