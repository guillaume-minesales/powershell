#Dependances pour affichage de message et compression
Add-Type -AssemblyName PresentationCore,PresentationFramework
Add-Type -AssemblyName System.IO.Compression.FileSystem

function Unzip
{
        param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

Unzip "program.zip" "program"

#Parametres pour l'affichage du message
$Message = "Audit Termine"
$Title = "BEA INFORMATIQUE"
$ButtonType = [System.Windows.MessageBoxButton]::OK
$MessageIcon = [System.Windows.MessageBoxImage]::Information

#Parametres pour l'execution d'aida
$Path = Get-Location
$user = whoami
$hote = hostname
$serial = (gwmi win32_bios).SerialNumber

#Execution d'aida
.\program\inventaire.exe /E /SUBJ "Rapport : $user - $hote - $serial" /HTML /SILENT /SAFE /CUSTOM $Path\App\bea.rpf

#Attente de 10 secondes
Start-Sleep -Seconds 10

#Affichage d'un message a l'utilisateur
$Result = [System.Windows.MessageBox]::Show($Message,$Title,$ButtonType,$MessageIcon)
